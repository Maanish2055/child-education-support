<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
session_start();
require_once 'dbconnection.php';

$emailErr = $passwordErr = '';

if (isset($_POST['submit'])) {
	$email = $password = '';
	$email = $_POST['email'];
	$password = $_POST['password'];

	if (empty($email)) {
		$emailErr = 'Email is required';
	}

	if (empty($password)) {
		$passwordErr = 'Password is required';
	}

	if ($emailErr == '' && $passwordErr == '') {
		$pass = md5($password);
		$query = mysqli_query($conn, "SELECT * from users where email='$email' and password='$pass'");
		$result = mysqli_fetch_array($query);
		$count = mysqli_num_rows($query);
		if ($count > 0) {
			$_SESSION['id'] = $result['id'];
			$_SESSION['email'] = $result['email'];
			header('location:users/create.php');
		} else {
			$emailErr = 'Invalid Credential';
		}
	}
}

?>



<!DOCTYPE html>
<html>

<head>
  <title>login</title>
  <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="login.css">

</head>

<body>

  <div class="container">
    <div class="header">
      <h1>login</h1>
    </div>
    <div class="main">
      <form action="" method="POST">
        <span>
          <i class="fa fa-user"></i>
          <?php 
          if(isset($emailErr != '')){
            echo $emailErr;
          }

           ?>
          <input type="text" placeholder="Username" name="email" class="username" >
        </span><br>
        <span>
          <i class="fa fa-lock"></i>
          <?php 
          if(isset($passwordErr != '')){
            echo $passwordErr;
          }

           ?>
          <input type="password" placeholder="password" name="password" class="password">
        </span><br>
        <button type="submit" name="submit">login</button><br>
        <a href="#" class="forget">Forget Password?</a><br>
        <button ><a href="form.html">Create New Account</a></button>


      </form>
    </div>
  </div>
</body>

</html>