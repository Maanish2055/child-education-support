<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('dbconnection.php');
$full_nameErr=$usernameErr=$passwordErr=$cpasswordErr=$genderErr=$dobErr=$addressErr=$contactErr=$emailErr=$imageErr='';
$full_name=$username=$password=$cpassword=$gender=$dob=$address=$contact=$email=$image='';
if(isset($_POST['submit'])){
  	$full_name = $_POST['full_name'];
  	$username = $_POST['username'];
	$password = $_POST['password'];
	$cpassword = $_POST['cpassword'];
	$uploadImage = false;
	$gender = $_POST['gender'] ?? 0;
  	$dob = $_POST['dob'];
	$address = $_POST['address'];
	$contact = $_POST['contact'];
	$email = $_POST['email'];
	$income = $_POST['income'] ?? 0;
  	
    if(empty($full_name)){
        $full_nameErr = '*Full name is required*';
    }
    
    if(empty($username)){
        $usernameErr = '*Username is required*';
    } 
    if(empty($password)){
        $passwordErr = '*Password is required*';
    }
    else if($password != $cpassword){
        $passwordErr = '*Password dont match*';
    }
	if(isset($_FILES['image']) && $_FILES['image']['name'] != ''){
        $uploadImage = true;
       $file_name = $_FILES['image']['name'];
       $file_type = $_FILES['image']['type'];
       $file_tmp_name = $_FILES['image']['tmp_name'];
       $file_size = $_FILES['image']['size'];
      
       if(!in_array($file_type,['image/jpeg','image/jpg','image/png'])){
           $imageErr = 'Only jpeg, png and jpg image is required';
       }
    }
	if(empty($gender)){
        $genderErr = '*Gender is required*';
    }
	if(empty($dob)){
        $dobErr = '*Date of Birth is required*';
    }
	if(empty($address)){
		$addressErr = '*Address is required*';
	}
	if(empty($contact)){
        $contactErr = '*Contact is required*';
    }
    if(empty($email)){
      $emailErr = '*Email is required*';
  	}
	if(empty($income)){
		$incomeErr = '*Income Resource is required*';
	}
    if($full_nameErr== '' && $usernameErr== '' && $passwordErr== '' && $cpasswordErr== '' && $imageErr== '' && $genderErr== '' && $dobErr== '' && $addressErr== '' && $contactErr== '' && $emailErr== ''){
        $password = md5($password);
			
        if($uploadImage){
	
            $fileName = time().'-'.$file_name;
            $target = '../image/user/'.$fileName;
            move_uploaded_file($file_tmp_name,$target);
            $mysql = mysqli_query($conn,"insert into users(full_name,username,password,gender,dob,address,contact,email,image='') values('$full_name','$username','$password','$image='','$gender','$dob','$address','$contact','$email')");
        }else{
            $mysql = mysqli_query($conn,"insert into users(full_name,username,password,gender,dob,address,contact,email) values('$full_name','$username','$password','$gender','$dob','$address','$contact','$email')");
        }

        if($mysql){
            $_SESSION['action'] = 'success';
            $_SESSION['message'] = 'Users Created Successfully!';
            header('location:userlogin.php');
        }
    }

  }

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="form.css">
    
</head>

<body>
    <section class="header">
        <nav>
           
            <div class="nav-links" id="navLinks">

                <i class="fa fa-times" onclick="hideMenu()"></i>
                <ul>
                    <li><a href=""><b>HOME</b></a></li>
                    <li><a href=""><b>ABOUT</b></a></li>
                    <li><a href=""><b>BLOG</b></a></li>
                    <li><a href=""><b>CONTACT</b></a></li>
                    <li class="has_submenu"> 
                    	<a href=""><b>SIGN IN</b></a>
                    	<ul class="submenu">
                    		<li><a href=""><b>Sponsor<b></a></li><br>
                            <li><a href=""><b>Child<b></a></li>
                    	</ul>

                    </li>

                </ul>
           </section>

    <div class="regform">
        <h1> Registration Form</h1>
    </div>
    <div class="main">
        
        <form method="POST">
            <div id="name">
             
                <h2 class="name"> Full Name  </h2>
                <input class="Address" type="text" name="fullname">
                <span class="error"><?php echo $full_nameErr?></span>
             
                <h2 class="name"> Username </h2>
             <input class="Address" type="text" name="username">
             <span class="error"><?php echo $usernameErr?></span>
             
             <h2 class="name"> Address </h2>
             <input class="Address" type="text" name="Address">
             <span class="error"><?php echo $addressErr?></span>
             
             <h2 class="name">Email</h2>
             <input class="Address" type="text" name="email">
             <span class="error"><?php echo $emailErr?></span>

             <h2 class="name">password</h2>
             <input class="Address" type="password" name="password">
             <span class="error"><?php echo $passwordErr?></span>
             
             <h2 class="name">confirm password</h2>
             <input class="Address" type="password" name="cpassword">
             <span class="error"><?php echo $cpasswordErr?></span>
             
            <h2 class="name">Phone</h2>
            
            <input class="Code" class="contact" type="text" name="area_code">
           
            <label class="area-code">Area Code</label>
            
            <input class="number" type="text" name="phone">
            <label class="phone-number">Phone Number</label>
            
            
            <h2 class="name">Gender</h2>
            <label class="radio">
            <input type="radio" name="gender" value="male"> Male
            
            <input type="radio" name="gender" value="female"> Female
            <input type="radio" name="gender" value="other"> Other
            </label> 
            

            <!-- <h2 class="name">DOB</h2>
             <input class="Address" type="date" name="dob" min="16"> -->

             <h2 class="name">Insert Picture</h2>
             <input class="Address" type="file" name="image">
             <span class="error"><?php echo $imageErr?></span>

            <h2 class="name"> massage </h2>
            <textarea name="" class="Address" cols="30" rows="5"></textarea>
            
             <h2 class="name"> Means of fund transfer </h2>
            <select class="option" name="fund">
                <option disabled="disabled" selected="selected">--Choose option--</option>
                <option>bank payment</option>
                <option>Cash</option>
                <option>cheque</option>
            </select> -->

            <button type="submit">Register</button>


        </form>
    </div>
</body>

</html>